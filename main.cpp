/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on May 31, 2016, 3:16 PM
 */

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <iostream>
#include <string>
#include <mraa.hpp>
#include <upm/pca9685.hpp>
#include <upm/mpu9250.hpp>
#include <upm/nunchuck.hpp>
#include <upm/lcm1602.hpp>
#include <upm/mpu60x0.hpp>

using namespace std;

/*
 * 
 */
#ifdef __cplusplus
extern "C" {
#endif
#include <signal.h>
#include <unistd.h>
#include <syslog.h>
#include <getopt.h>
#include <string.h>
#ifdef __cplusplus
}
#endif 

int Running = 1;
static std::string argsString = "\n"
        "  --i2cbus ID: ID is 0 or 1\n"
        "\n\n"
        ;

/*
 * 
 */
void signal_callback_handler(int signum) {

    Running = 0;
}

float map(double x, double in_min, double in_max, double out_min, double out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void testLCD(upm::Lcm1602* lcd) {

    //upm::Lcm1602 lcd2(i2cbus_id, 0x27);
    // upm::Lcm1602* lcd = new upm::Lcm1602(i2cbus_id, 0x27);
    lcd->home();
    lcd->setCursor(0, 0);
    lcd->write("1Hello World");
    sleep(3);
    lcd->clear();
    sleep(4);
    lcd->write("ended");
    sleep(3);
    //delete lcd;

}


#define MIN_X 300 // moving left
#define MAX_X 700 // moving right
#define MIN_Y 290 // moving backward
#define MAX_Y 680 // moving forward
#define MIN_Z 331 // moving backward
#define MAX_Z 700 // moving forward

void testNunchuck(int i2cbus_id) {
    upm::NUNCHUCK nunchuck(i2cbus_id);
    while (Running) {

        //printf("direction: %.2f, heading: %.2f\n",hmc.direction(),hmc.heading());
        nunchuck.update();
        printf("x=%03ld,y=%03ld,z=%03ld\n",
                (long) map(nunchuck.accelX, MIN_X, MAX_X, 0, 180),
                (long) map(nunchuck.accelY, MIN_Y, MAX_Y, 0, 180),
                //nunchuck.accelY,
                (long) map(nunchuck.accelZ, MIN_Z, MAX_Z, 0, 180));
        printf("x=%03d,y=%03d,z=%03d\n", nunchuck.accelX,
                nunchuck.accelY, nunchuck.accelZ);
        usleep(15000);
    }
}

#define SERVOMIN  110 // this is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  560 // this is the 'maximum' pulse length count (out of 4096)
#define CHANNEL 0

void testPCA9685(int i2cbus_id) {
    upm::PCA9685 *pca9685 = new upm::PCA9685(i2cbus_id, 0x40);

    pca9685->setModeSleep(true);
    // setup a period of 50Hz
    pca9685->setPrescaleFromHz(50);

    pca9685->ledOnTime(PCA9685_ALL_LED, 0);
    pca9685->ledOffTime(PCA9685_ALL_LED, map(90, 0, 180, SERVOMIN, SERVOMAX));
    // wake device up
    pca9685->setModeSleep(false);
    while (Running) {
        pca9685->ledOffTime(CHANNEL, map(0, 0, 180, SERVOMIN, SERVOMAX));
        usleep(520000);
        pca9685->ledOffTime(CHANNEL, map(180, 0, 180, SERVOMIN, SERVOMAX));
        usleep(520000);
    }
    pca9685->ledOffTime(CHANNEL, map(90, 0, 180, SERVOMIN, SERVOMAX));
    sleep(1);

    delete pca9685;
}

float getOrientation(float ax, float ay, float az, float mx, float my, float mz,
        float *heading, float *pitch, float *roll) {
    float i2cHeading, i2cPitch, i2cRoll;

    float const PI_F = 3.14159265F;

    // i2cRoll: Rotation around the X-axis. -180 <= i2cRoll <= 180                                          
    // a positive i2cRoll angle is defined to be a clockwise rotation about the positive X-axis                                                                                                          
    //                       y                                                                           
    //      i2cRoll = atan2(---)                                                                         
    //                       z                                                                           
    // where:  y, z are returned value from accelerometer sensor                                      
    i2cRoll = (float) atan2(ay, az);

    // i2cPitch: Rotation around the Y-axis. -180 <= i2cRoll <= 180                                         
    // a positive i2cPitch angle is defined to be a clockwise rotation about the positive Y-axis                                                                                                   
    //                                 -x                                                             
    //      i2cPitch = atan(-------------------------------)                                             
    //                    y * sin(i2cRoll) + z * cos(i2cRoll)                                                                                                                                             
    // where:  x, y, z are returned value from accelerometer sensor                                   
    if (ay * sin(i2cRoll) + az * cos(i2cRoll) == 0)
        i2cPitch = ax > 0 ? (PI_F / 2) : (-PI_F / 2);
    else
        i2cPitch = (float) atan(-ax / (ay * sin(i2cRoll) + az * cos(i2cRoll)));

    // i2cHeading: Rotation around the Z-axis. -180 <= i2cRoll <= 180                                       
    // a positive i2cHeading angle is defined to be a clockwise rotation about the positive Z-axis                                                                                                   
    //                                       z * sin(i2cRoll) - y * cos(i2cRoll)                            
    //   i2cHeading = atan2(--------------------------------------------------------------------------)  
    //                    x * cos(i2cPitch) + y * sin(i2cPitch) * sin(i2cRoll) + z * sin(i2cPitch) * cos(i2cRoll))                                                                                  
    // where:  x, y, z are returned value from magnetometer sensor                                    
    i2cHeading = (float) atan2(mz * sin(i2cRoll) - my * cos(i2cRoll), mx * cos(i2cPitch) + my * sin(i2cPitch) * sin(i2cRoll) + mz * sin(i2cPitch) * cos(i2cRoll));

    // Convert angular data to degree 
    *roll = i2cRoll = -i2cRoll * 180.0 / PI_F;
    *pitch = i2cPitch = i2cPitch * 180.0 / PI_F;
    *heading = i2cHeading = -i2cHeading * 180.0 / PI_F;
    //    std::cout << "i2cRoll: " << (int) i2cRoll
    //            << ", i2cPitch: " << (int) i2cPitch
    //            << ", i2cHeading: " << (int) i2cHeading
    //            << std::endl;
    //
    //    usleep(500000);
}
void testMPU6050(int i2cbus_id) {
        float ax, ay, az;
    float gx, gy, gz;
    float mx, my, mz;
    float heading, pitch, roll;
    upm::MPU60X0 *sensor = new upm::MPU60X0(i2cbus_id);
     sensor->init();
    while (Running) {
        sensor->update();
        sensor->getGyroscope(&gx, &gy, &gz);
        sensor->getAccelerometer(&ax, &ay, &az);
        getOrientation(ax, ay, az, mx, my, mz, &heading, &pitch, &roll);
        

        std::cout << "Accelerometer: ";
        std::cout << "AX: " << ax << ", AY: " << ay << ", AZ: " << az << std::endl;
        std::cout << "Gryoscope:     ";
        std::cout << "GX: " << gx << ", GY: " << gy << ", GZ: " << gz << std::endl;
        std::cout << "Magnetometer:  ";
        std::cout << "MX = " << mx << ", MY = " << my << ", MZ = " << mz << std::endl;
        std::cout << "Temperature:   " << sensor->getTemperature() << std::endl;
        std::cout << std::endl;
        //10 ms wait
        usleep(10000);
    }
    std::cout << "Exiting..." << std::endl;
    delete sensor;
}
void testMPU9250(int i2cbus_id) {
    float ax, ay, az;
    float gx, gy, gz;
    float mx, my, mz;
    float heading, pitch, roll;


    upm::MPU9250 *sensor = new upm::MPU9250(i2cbus_id);
    sensor->init();
    while (Running) {
        sensor->update();
        sensor->getGyroscope(&gx, &gy, &gz);
        sensor->getAccelerometer(&ax, &ay, &az);
        sensor->getMagnetometer(&mx, &my, &mz);
        getOrientation(ax, ay, az, mx, my, mz, &heading, &pitch, &roll);
        

        std::cout << "Accelerometer: ";
        std::cout << "AX: " << ax << ", AY: " << ay << ", AZ: " << az << std::endl;
        std::cout << "Gryoscope:     ";
        std::cout << "GX: " << gx << ", GY: " << gy << ", GZ: " << gz << std::endl;
        std::cout << "Magnetometer:  ";
        std::cout << "MX = " << mx << ", MY = " << my << ", MZ = " << mz << std::endl;
        std::cout << "Temperature:   " << sensor->getTemperature() << std::endl;
        std::cout << std::endl;

        //10 ms wait
        usleep(10000);
    }
    std::cout << "Exiting..." << std::endl;


    delete sensor;

}

void testNativePWM(int pin) {
    mraa::Pwm* pwm;
    pwm = new mraa::Pwm(pin);
    pwm->enable(true);
    pwm->period_ms(20);
    std::cout << "max: " << pwm->max_period() << "min: " << pwm->min_period() << std::endl;
    for (int i = 0; i < 5; i++) {

        pwm->pulsewidth_us(550);
        //pwm->write(1);
        usleep(200000);
        pwm->pulsewidth_us(2400);
        //pwm->write(1);
        usleep(200000);
    }
    pwm->enable(false);
    delete pwm;
}

void testgpio() {
    int pins[] = {35, 36, 37, 38};

    mraa_gpio_context gpio1, gpio2;

    for (int i = 0; i < 4; i += 2) {
        gpio1 = mraa_gpio_init(pins[i]);
        gpio2 = mraa_gpio_init(pins[i + 1]);
        //printf("%d %d\n", pins[i], pins[i + 1]);
        mraa_gpio_dir(gpio1, MRAA_GPIO_OUT);
        mraa_gpio_dir(gpio2, MRAA_GPIO_OUT);

        mraa_gpio_write(gpio1, 1);
        mraa_gpio_write(gpio2, 0);
        sleep(5);

        mraa_gpio_write(gpio1, 0);
        mraa_gpio_write(gpio2, 1);
        sleep(5);
        mraa_gpio_write(gpio1, 0);
        mraa_gpio_write(gpio2, 0);
        //
        if (1) {

            mraa_gpio_close(gpio1);
            mraa_gpio_close(gpio2);
        }
        //break;
    }

}
#define MAX_DUTY_MS 2500
#define MIN_DUTY_MS 550
#define PWM(angle) ((int) (MIN_DUTY_MS + (MAX_DUTY_MS - MIN_DUTY_MS) * angle / 180.0))

int testUART(std::string uart_port) {
    mraa_uart_context uart;
    uart = mraa_uart_init_raw(uart_port.c_str());
    mraa_uart_set_baudrate(uart, 115200);
    mraa_uart_set_mode(uart, 8, MRAA_UART_PARITY_NONE, 1);

    if (uart == NULL) {
        fprintf(stderr, "UART failed to setup %s\n", uart_port.c_str());
        return EXIT_FAILURE;
    }

    const int delay_ms = 50;
    for (int j = 0; j < 5; j++) {
        for (int angle = 0; angle <= 180; angle += 10) {
            char buffer[64];
            sprintf(buffer, "#0 P%d T%d\r\n", PWM(angle), delay_ms);
            //std::cout << i << std::endl;
            std::cout << buffer;
            mraa_uart_write(uart, buffer, strlen(buffer));
            usleep(delay_ms * 1000);
        }
    }
    mraa_uart_stop(uart);

    return 0;
}

int main(int argc, char** argv) {
    int c, option_index = 0;
    int i2cbus_id = 0;
    std::string optionName;
    static struct option long_options[] = {
        {"i2cbus", required_argument, 0, 0},
        {0, 0, 0, 0}
    };
    upm::Lcm1602* lcd = NULL;

    opterr = 0;
    while ((c = getopt_long(argc, argv, "?",
            long_options, &option_index)) != -1) {
        switch (c) {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0)
                    break;
                optionName = long_options[option_index].name;
                if (optionName == "i2cbus") {
                    i2cbus_id = atoi(optarg);
                    if (i2cbus_id < 0 || i2cbus_id > 1) {
                        std::cout << "Invalid i2cbus ID!" << std::endl;
                        std::cout << "Usage: " << argv[0] << argsString;
                        exit(0);
                    }
                }
                break;
            case '?':
                std::cout << "Usage: " << argv[0] << argsString;
                exit(0);
                break;
            default:
                break;
        }
    }
    signal(SIGINT, signal_callback_handler);
    signal(SIGTERM, signal_callback_handler);


    mraa_init();
    fprintf(stdout, "MRAA Version: %s ", mraa_get_version());
    fprintf(stdout, "on %s\n", mraa_get_platform_name());
//    try {
//        lcd = new upm::Lcm1602(i2cbus_id, 0x27);
//        lcd->backlightOn();
//        lcd->home();
//        lcd->write("program STARTed!");
//    } catch (...) {
//        lcd = NULL;
//        std::cout << "program STARTed!\n" << std::endl;
//    }

    //testgpio();
    //testUART("/dev/ttyUSB0");
    //testNativePWM(12);
    //testNunchuck(i2cbus_id);
    //testLCD(lcd);
    //testMPU9250(i2cbus_id);
    //testMPU6050(0);
    //testPCA9685(i2cbus_id);   

//    if (lcd != NULL) {
//        lcd->clear();
//        lcd->write("program ENDed!");
//        sleep(3);
//        lcd->backlightOff();
//    } else {
//        std::cout << "program ENDed!\n" << std::endl;
//    }
//    delete lcd;
    mraa_deinit();

    return EXIT_SUCCESS;
}


